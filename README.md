### Installing configuration with `oc`
```
oc create configmap shib-config \
  --from-file=okd/dev/shibboleth2.xml \
  --from-file=okd/attribute-map.xml

oc create configmap apache-config \
  --from-file=okd/dev/servername.conf \
  --from-file=okd/dev/application.conf

oc create configmap jupyter-env --from-env-file=.env

oc create -f okd/deploymentConfigShib.yml
oc create -f okd/dev/deploymentConfigWeb.yml
oc create -f okd/serviceShib.yml
oc create -f okd/serviceWeb.yml
oc create -f okd/dev/routeApp.yml
```
